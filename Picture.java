import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Picture {
	
	private BufferedImage image;

	//Load the picture (on a file) on a BufferedImage
	public void charger(File file) {
		try {
			image = ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Save the picture called "Modified_OriginalName.OriginalExtension" in the same folder than the original
	public void enregistrer(String path,String nom) {
		File chemin = new File(path+"\\Modified_"+nom);
		try {
			if(nom.contains(".png"))
				ImageIO.write(this.image, "png",chemin);
			if(nom.contains(".jpg"))
				ImageIO.write(this.image, "jpg",chemin);
			System.out.println("Enregistrement effectu�");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Put grey level on each original pixel, and store it in the Picture
 	public void niveauxDeGris() {
 		int color;
 		Couleur couleur = new Couleur(0);
 		for(int i=0;i<this.image.getWidth();i++) {
 			for(int j=0;j<this.image.getHeight();j++) {	
 				color =this.image.getRGB(i, j);
 		 		couleur.setCouleur(color);
 		 		couleur.toNiveauGris();
 		 		this.image.setRGB(i, j, couleur.toValidColor());
 			}
 		}
 			
 	}
 	//Transform each pixels in the opposite of the original, and save it in the Picture
 	public void toNegative() {
 		BufferedImage imageTampon = new BufferedImage(this.image.getWidth(), this.image.getHeight(),this.image.getType());
 		int color;
 		Couleur couleur = new Couleur(0);
 		for(int i=0;i<this.image.getWidth();i++) {
 			for(int j=0;j<this.image.getHeight();j++) {	
 				color =this.image.getRGB(i, j);
 		 		couleur.setCouleur(color);
 		 		couleur.toNegative();
 		 		imageTampon.setRGB(i, j, couleur.toValidColor());
 			}
 		}
 			this.image=imageTampon;
 	}
 	
 	//Put the binarized image in the Picture
 	//Parameters are the int form of the color, they are convert in Couleur using the static final list in the Couleur Object
 	public void binariser(int c1, int c2) {
 		this.image=this.imageBinarisee(new Couleur(Couleur.LISTE_COULEUR_STRINGS[c1-1]),new Couleur(Couleur.LISTE_COULEUR_STRINGS[c2-1]));
 	}
 	
 	//Return a BufferedImage which is the binarized image of the original, using the Couleur parameters 
 	public BufferedImage imageBinarisee(Couleur c1, Couleur c2) {
 		BufferedImage imageTampon = new BufferedImage(this.image.getWidth(), this.image.getHeight(),this.image.getType());
 		int color;
 		Couleur couleur = new Couleur(0);
 		for(int i=0;i<this.image.getWidth();i++) {
 			for(int j=0;j<this.image.getHeight();j++) {	
 				color =this.image.getRGB(i, j);
 		 		couleur.setCouleur(color);
 		 		couleur.toBinaire(c1,c2);
 		 		imageTampon.setRGB(i, j, couleur.toValidColor());
 			}
 		}
 		return imageTampon;
 	}

 	//Wharolize the image in the Picture using 8 int symbolizing colors
 	//It put each binarized image from the list newImage in a new BufferedImage 2 times larger
 	public void wharoliser(int c1, int c2, int c3, int c4, int c5, int c6, int c7, int c8) {
 		BufferedImage newImage = new BufferedImage(this.image.getWidth()*2, this.image.getHeight()*2,this.image.getType());
 		ArrayList<BufferedImage> listImages = createList(c1, c2, c3, c4, c5, c6, c7, c8) ;
 		for(int k=0;k<4;k++) {
 			switch(k) {
 			case 0:
 				for(int i=0;i<this.image.getWidth();i++) {
 		 			for(int j=0;j<this.image.getHeight();j++) {	
 		 				newImage.setRGB(i, j, listImages.get(0).getRGB(i, j));
 		 			}
 		 		}
 				System.out.println(".");
 				break;
 			case 1:
 				for(int i=this.image.getWidth();i<this.image.getWidth()*2;i++) {
 		 			for(int j=0;j<this.image.getHeight();j++) {	
 		 				newImage.setRGB(i, j, listImages.get(1).getRGB(i-this.image.getWidth(), j));
 		 			}
 		 		}
 				System.out.println("..");
 				break;
 			case 2:
 				for(int i=0;i<this.image.getWidth();i++) {
 		 			for(int j=this.image.getHeight();j<this.image.getHeight()*2;j++) {	
 		 				newImage.setRGB(i, j, listImages.get(2).getRGB(i, j-this.image.getHeight()));
 		 			}
 		 		}
 				System.out.println("...");
 				break;
 			case 3:
 				for(int i=this.image.getWidth();i<this.image.getWidth()*2;i++) {
 		 			for(int j=this.image.getHeight();j<this.image.getHeight()*2;j++) {	
 		 				newImage.setRGB(i, j, listImages.get(3).getRGB(i-this.image.getWidth(), j-this.image.getHeight()));
 		 			}
 		 		}
 				break;
 			}
 		}
 		this.image=newImage;
 	}
 	
 	//return a list of 4 binarized BufferedImages using colors in parameters, and the function imageBinarisee()
 	public ArrayList<BufferedImage> createList(int c1, int c2, int c3, int c4, int c5, int c6, int c7, int c8){
 		ArrayList<BufferedImage> listImages = new ArrayList<BufferedImage>();
 		BufferedImage imageTampon = new BufferedImage(this.image.getWidth(), this.image.getHeight(),this.image.getType());;
 		imageTampon = new BufferedImage(this.image.getWidth(), this.image.getHeight(),this.image.getType());
 		
 		imageTampon = this.imageBinarisee(new Couleur(Couleur.LISTE_COULEUR_STRINGS[c1-1]), new Couleur(Couleur.LISTE_COULEUR_STRINGS[c2-1]));
 		listImages.add(imageTampon);
 		
 		imageTampon = this.imageBinarisee(new Couleur(Couleur.LISTE_COULEUR_STRINGS[c3-1]), new Couleur(Couleur.LISTE_COULEUR_STRINGS[c4-1]));
 		listImages.add(imageTampon);

 		imageTampon = this.imageBinarisee(new Couleur(Couleur.LISTE_COULEUR_STRINGS[c5-1]), new Couleur(Couleur.LISTE_COULEUR_STRINGS[c6-1]));
 		listImages.add(imageTampon);
 		
 		imageTampon = this.imageBinarisee(new Couleur(Couleur.LISTE_COULEUR_STRINGS[c7-1]), new Couleur(Couleur.LISTE_COULEUR_STRINGS[c8-1]));
 		listImages.add(imageTampon);
 		
 		return listImages;
 	}
 	
}
